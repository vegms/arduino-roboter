#include<Servo.h>
#include<NewPing.h>
//unsere L298N Kontroller Pins
const int LMV = 5;                           //LinkerMotor nach Vorne
const int LMR = 4;                           //LinkerMotor zurück
const int RMV = 3;                           //RechterMotor Vorne
const int RMR = 2;                           //RechterMotor zurück
//Die UltrasonicSensor Pins
#define trig_pin A1                          //Analoger output Ausloeser Sonar
#define echo_pin A2                          //Analoger input Sonar Echo empfangen
#define max_Entf 200                         //Maximale Entfernung
boolean FahrtNachVorn = false;
int Entfernung = 100;
const int servo_mitte = 83;                  //Servo mittig positionieren, kalibrierbar
NewPing Sonar(trig_pin, echo_pin, max_Entf); //Sonar Funktionen
Servo Schrittmotor;                          //mein Servo Name

void setup() {
  pinMode(RMV , OUTPUT);
  pinMode(LMV , OUTPUT);
  pinMode(LMR , OUTPUT);
  pinMode(RMR , OUTPUT);

  Schrittmotor.attach(11);                   //mein Servo Pin
  Schrittmotor.write(servo_mitte);           //Servo Pin bewegt sich auf 90 Grad
  delay(2000);                               //Programm steht 2 Sekunden
  Entfernung = LiesPing();                   //Liest den Ping und füllt diesen an die stelle von der Variablen Entfernung
  delay(100);

}
void loop() {
  int EntfernungRechts = 0;
  int EntfernungLinks = 0;
  delay(50);
  
  if(Entfernung<=20){
    Stopp();
    delay(300);
    ZuRFahren();
    delay(200);
    Stopp();
    delay(300);
    EntfernungRechts = GuckRechts();
    delay(300);
    EntfernungLinks = GuckLinks();
    delay(300);

    if(Entfernung >= EntfernungLinks){
      NachRechtsDrehen();
      Stopp();
      }
    else{
      NachLinksDrehen();
      Stopp();
    }
 }
 else{
   NachVorneFahren();
   }
 Entfernung = LiesPing();
} //voidLoop ist zu 

int GuckRechts(){
  Schrittmotor.write(servo_mitte - 45);         //den Servo 45 Grad von vorne nach rechts drehen
  delay(500);
  int EntfernungRechts = LiesPing();                    //  int Entfernung = LiesPing();
  delay(100);
  Schrittmotor.write(servo_mitte);
  return Entfernung;
  delay(100);
}
int GuckLinks(){
  Schrittmotor.write(servo_mitte + 45);
  delay(500);
  int EntfernungLinks = LiesPing();                      // int Entfernung = LiesPing();
  delay(100);
  Schrittmotor.write(servo_mitte);
  return Entfernung;
  delay(100);
}
int LiesPing(){
  delay(70);
  int cm = Sonar.ping_cm();
  if (cm==0){                                    // Vergleich, ob 0 und die cm Zahl gleich sind
    cm=250;
  }
  return cm;
}

void Stopp(){
  digitalWrite(RMV , LOW);
  digitalWrite(LMV , LOW);
  digitalWrite(LMR , LOW);
  digitalWrite(RMR , LOW);
}
void NachVorneFahren(){
  if(!FahrtNachVorn){
    FahrtNachVorn = true;
    digitalWrite(LMV, HIGH);
    digitalWrite(RMV, HIGH);
    digitalWrite(LMR, LOW);
    digitalWrite(RMR, LOW);
    }
}
void ZuRFahren(){
  FahrtNachVorn = false;
  digitalWrite(LMR, HIGH);
  digitalWrite(RMR, HIGH);
  digitalWrite(LMV, LOW);
  digitalWrite(RMV, LOW);
}
void NachRechtsDrehen(){
  digitalWrite(LMV, HIGH);
  digitalWrite(RMR, HIGH);
  digitalWrite(LMR, LOW);
  digitalWrite(RMV, LOW);  
  delay(350);
  digitalWrite(LMV, HIGH);
  digitalWrite(RMV, HIGH);
  digitalWrite(LMR, LOW);
  digitalWrite(RMR, LOW);
}
void NachLinksDrehen(){
  
  digitalWrite(LMR, HIGH);
  digitalWrite(RMV, HIGH);
  digitalWrite(LMV, LOW);
  digitalWrite(RMR, LOW);  
  delay(350);
  digitalWrite(LMV, HIGH);
  digitalWrite(RMV, HIGH);
  digitalWrite(LMR, LOW);
  digitalWrite(RMR, LOW);
  
}
