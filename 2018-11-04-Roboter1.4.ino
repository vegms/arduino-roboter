//Speicherverbrauch Ursprungscode
//Der Sketch verwendet 3256 Bytes (10%) des Programmspeicherplatzes. Das Maximum sind 32256 Bytes.
//Globale Variablen verwenden 75 Bytes (3%) des dynamischen Speichers, 1973 Bytes für lokale Variablen verbleiben. Das Maximum sind 2048 Bytes.

//Speicherverbrauch aktuell:
//Der Sketch verwendet 3138 Bytes (9%) des Programmspeicherplatzes. Das Maximum sind 32256 Bytes.
//Globale Variablen verwenden 76 Bytes (3%) des dynamischen Speichers, 1972 Bytes für lokale Variablen verbleiben. Das Maximum sind 2048 Bytes.

#include<Servo.h>
#include<NewPing.h>
//unsere L298N Kontroller Pins
const int LMV = 5;                           //LinkerMotor nach Vorne
const int LMR = 4;                           //LinkerMotor zurück
const int RMV = 3;                           //RechterMotor Vorne
const int RMR = 2;                           //RechterMotor zurück
//Die UltrasonicSensor Pins
#define trig_pin A1                          //Analoger output Ausloeser Sonar
#define echo_pin A2                          //Analoger input Sonar Echo empfangen
#define max_Entf 200                         //Maximale Entfernung
int Entfernung = 100;
int EntfernungRechts = 0;
int EntfernungLinks = 0;
NewPing Sonar(trig_pin, echo_pin, max_Entf); //Sonar Funktionen
// Servo
const int servo_mitte = 83;                  //Servo mittig positionieren, kalibrierbar
Servo Schrittmotor;                          //mein Servo Name

void setup() {
  pinMode(RMV , OUTPUT);
  pinMode(LMV , OUTPUT);
  pinMode(LMR , OUTPUT);
  pinMode(RMR , OUTPUT);

  Schrittmotor.attach(11);                   //mein Servo Pin
  Schrittmotor.write(servo_mitte);           //Servo Pin bewegt sich auf 90 Grad
  delay(2000);                               //Programm steht 2 Sekunden
  Entfernung = LiesPing();                   //Liest den Ping und füllt diesen an die stelle von der Variablen Entfernung

}
void loop() {
    if(Entfernung<=20){
    Stopp();
    ZuRFahren();
    Stopp();
    EntfernungRechts = GuckRechts();
    EntfernungLinks = GuckLinks();
    
    if(EntfernungRechts <= EntfernungLinks){
      NachRechtsDrehen();
      Stopp();
      }
    else{
      NachLinksDrehen();
      Stopp();
    }
 }
 else{
   NachVorneFahren();
   }
 Entfernung = LiesPing();
} //voidLoop ist zu 

int GuckRechts(){
  Schrittmotor.write(servo_mitte - 45);      //den Servo 45 Grad von vorne nach rechts drehen
  delay(500);
  Entfernung = LiesPing();
  Schrittmotor.write(servo_mitte);
  delay(300);
  return Entfernung;
}
int GuckLinks(){
  Schrittmotor.write(servo_mitte + 45);
  delay(500);
  Entfernung = LiesPing();
  Schrittmotor.write(servo_mitte);
  delay(300);
  return Entfernung;
}
int LiesPing(){
//  delay(70);
  int cm = Sonar.ping_cm();
  if (cm==0){                                 // Vergleich, ob 0 und die cm Zahl gleich sind
    cm=250;
  }
  delay(100);
  return cm;
}

void Stopp(){
  digitalWrite(RMV , LOW);
  digitalWrite(LMV , LOW);
  digitalWrite(LMR , LOW);
  digitalWrite(RMR , LOW);
  delay(300);
}
void NachVorneFahren(){
  digitalWrite(LMV, HIGH);
  digitalWrite(RMV, HIGH);
  digitalWrite(LMR, LOW);
  digitalWrite(RMR, LOW);
}
void ZuRFahren(){
  digitalWrite(LMR, HIGH);
  digitalWrite(RMR, HIGH);
  digitalWrite(LMV, LOW);
  digitalWrite(RMV, LOW);
  delay(200);
}
void NachRechtsDrehen(){
  digitalWrite(LMV, HIGH);
  digitalWrite(RMR, HIGH);
  digitalWrite(LMR, LOW);
  digitalWrite(RMV, LOW);  
  delay(350);
}
void NachLinksDrehen(){
  digitalWrite(LMR, HIGH);
  digitalWrite(RMV, HIGH);
  digitalWrite(LMV, LOW);
  digitalWrite(RMR, LOW);  
  delay(350);  
}
